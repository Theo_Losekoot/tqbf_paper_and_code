/*
 * generated by Xtext 2.19.0
 */
package org.xtext.spa.qbf


/**
 * Use this class to register components to be used at runtime / without the Equinox extension registry.
 */
class QbfRuntimeModule extends AbstractQbfRuntimeModule {
}
