/*
 * generated by Xtext 2.19.0
 */
package org.xtext.spa.qbf


/**
 * Initialization support for running Xtext languages without Equinox extension registry.
 */
class QbfStandaloneSetup extends QbfStandaloneSetupGenerated {

	def static void doSetup() {
		new QbfStandaloneSetup().createInjectorAndDoEMFRegistration()
	}
}
