package org.xtext.spa.qbf.code

import java.io.BufferedWriter
import java.io.FileReader
import java.io.FileWriter
import java.io.IOException
import org.eclipse.emf.ecore.util.EcoreUtil
import org.xtext.spa.qbf.qbf.Formula

class Mein 
{
	
	def static void main(String[] args)
	{
		if(args.size() < 3)
		{
			throw new Error("Usage : java -jar tqbfparser <input file> <output_file> <method (\"qdimacs\" or \"dimacs\")>")
		}
		
		val filename = args.get(0)
		//val filename = "src/formula.qbf"
		println("filename: " + filename)
		val read = new Reader();
		read.XtextParser();
		
		val ast = read.parse( new FileReader(filename)) as Formula;
		//pprint(ast);
		Transformations.pretty_print(ast);
		println()
		val output = args.get(1)
		val method = args.get(2)
		if (method.equals("qdimacs"))
			get_qdimacs(ast, output)
		else if (method.equals("dimacs"))
			naive_convert_to_sat(ast, output)
		else 
			print("invalid method name")
	}
	
	
	def static get_qdimacs(Formula ast, String filename_dimacs)
	{
		
		val quantifiers = Transformations.get_quantifiers(ast)
		val formula = Transformations.get_formula(ast)
		
		println("formula : ")
		Transformations.pretty_print(formula)
		println()


		val sat_formula_cnf = Transformations.formula_to_equivalent_CNF(EcoreUtil.copy(formula))
		
		val qsat_formula_cnf_dimacs = Transformations.prop_to_Qdimacs(sat_formula_cnf, quantifiers)
		println("qbf -> sat formula -> cnf -> dimacs : ")
		println(qsat_formula_cnf_dimacs)
		println()
		
		//val filename_dimacs = "src/output.cnf"
		save(filename_dimacs, qsat_formula_cnf_dimacs)
		
		println()
	}
	
	
	def static naive_convert_to_sat(Formula ast, String filename_dimacs)
	{
		val quantifiers = Transformations.get_quantifiers(ast)
		val formula = Transformations.get_formula(ast)
		
		println("formula : ")
		Transformations.pretty_print(formula)
		println()


		val sat_formula = Transformations.TQBF_to_SAT(quantifiers, EcoreUtil.copy(formula), "#")
		val sat_formula_cnf = Transformations.formula_to_equivalent_CNF(EcoreUtil.copy(sat_formula))
		val sat_formula_cnf_nonT = Transformations.remove_truth_values_from_cnf(sat_formula_cnf)


		
		val sat_formula_cnf_dimacs = Transformations.prop_to_dimacs(EcoreUtil.copy(sat_formula_cnf_nonT))
		println("qbf -> sat formula -> cnf -> dimacs : ")
		println(sat_formula_cnf_dimacs)
		println()
		
		//val filename_dimacs = "src/output.cnf"
		save(filename_dimacs, sat_formula_cnf_dimacs)
		
		println()
	}
	
	
	def static save(String filename, String content)
	{
		try 
		{
	    	val writer = new BufferedWriter(new FileWriter(filename));
	    	writer.write(content + "\n");
	     
    		writer.close();
		}
		catch (IOException e)
		{
			print("error writing in " + filename + " : " + e)
		}
	}
}




		/*
		val formula_cnf = Transformations.formula_to_equivalent_CNF(EcoreUtil.copy(formula))		
		println("formula_CNF : ")
		Transformations.pretty_print(formula_cnf)
		println()
		
		println("Quantifiers : ")
		quantifiers.forEach([x | Transformations.pretty_print(x)])
		println()
		
		val dimacs_formula = Transformations.prop_to_dimacs(formula_cnf)
		println("dimacs_formula : ")
		println(dimacs_formula)
		println()
		
		
		println("qbf -> sat formula : ")
		Transformations.pretty_print(sat_formula)
		println()
		
		
		println("qbf -> sat formula -> cnf : ")
		Transformations.pretty_print(sat_formula_cnf)
		println()
		
		println("qbf -> sat formula -> cnf -> non T : ")
		Transformations.pretty_print(sat_formula_cnf_nonT)
		println()
		*/