package org.xtext.spa.qbf.code;

import java.io.IOException;
import java.util.ArrayList;
import java.util.stream.Collectors;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.IParseResult;
import org.eclipse.xtext.parser.IParser;
import org.eclipse.xtext.parser.ParseException;
import org.xtext.spa.qbf.QbfStandaloneSetup;

import com.google.inject.Inject;
import com.google.inject.Injector;

public class Reader
{
    @Inject
    private IParser parser;
    
 
    public void XtextParser() 
    {
        setupParser();
    }
 
    private void setupParser()
    {
        Injector injector = new QbfStandaloneSetup().createInjectorAndDoEMFRegistration();
        injector.injectMembers(this);

        
        ArrayList<Integer> product = new ArrayList();
        
        product.stream().map(n->n*2).collect(Collectors.toList()); 

    }
 
    /**
     * Parses data provided by an input reader using Xtext and returns the root node of the resulting object tree.
     * @param reader Input reader
     * @return root object node
     * @throws IOException when errors occur during the parsing process
     */
    public EObject parse(java.io.Reader reader) throws IOException
    {
        IParseResult result = parser.parse(reader);
        if(result.hasSyntaxErrors())
        {
            throw new ParseException("Provided input contains syntax errors.");
        }
        return result.getRootASTElement();
    }
}