package org.xtext.spa.qbf.code

import java.util.ArrayList
import java.util.List
import java.util.Set
import java.util.stream.Collectors
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.util.EcoreUtil
import org.xtext.spa.qbf.qbf.And
import org.xtext.spa.qbf.qbf.BiImpl
import org.xtext.spa.qbf.qbf.Expression
import org.xtext.spa.qbf.qbf.Formula
import org.xtext.spa.qbf.qbf.Impl
import org.xtext.spa.qbf.qbf.Nand
import org.xtext.spa.qbf.qbf.Not
import org.xtext.spa.qbf.qbf.Or
import org.xtext.spa.qbf.qbf.QbfFactory
import org.xtext.spa.qbf.qbf.Quantifier
import org.xtext.spa.qbf.qbf.TypeQuantifier

class Transformations
{
	
	static val factory = QbfFactory.eINSTANCE	
	

	def static List<Expression> get_clauses(Expression expression)
	{
		if(expression instanceof Or)
		{
			val list = new ArrayList<Expression>();
			list.add(expression)
			return list	
		}
		else if (expression instanceof And)
		{			
			var l1 = get_clauses((expression as And).left)
			val l2 = get_clauses((expression as And).right)
			
			l1.addAll(l2)
						
			return l1
		}
		else if (expression instanceof Not)
		{
			val list = new ArrayList<Expression>();
			list.add(expression)
			return list	
		}
		else if (is_atom(expression))
		{
			val list = new ArrayList<Expression>();
			list.add(expression)
			return list	
		}
		else
		{
			print("formula : ")
			pretty_print(expression)
			println()
			
			throw new Error("gaaah")
		}
	}
	
	def static private <TypeA,TypeB> List< Pair<TypeA, TypeB> > list_product(List<TypeA> l1, List<TypeB> l2)
	{
    	var list = new ArrayList< Pair <TypeA, TypeB> > ()
    	for (e1 : l1)
    	{
    		for (e2 : l2)
    		{
    			list.add(new Pair<TypeA, TypeB> (e1, e2))
    		}
    	}
    	
    	return list
    }		
    	
	
	def static Expression get_true()
	{
		val truu = factory.createExpression()
		truu.setVal("true")
		return truu
	}
	
	def static Expression get_GNAA()
	{
		val truu = factory.createExpression()
		truu.setVal("GNAAA")
		return truu
	}
	
	def static Expression get_false()
	{
		val falss = factory.createExpression()
		falss.setVal("false")
		return falss
	}
	
								
	def static Expression get_conjunction(Expression e1, Expression e2)
	{
		val the_and = factory.createAnd();
		the_and.setLeft(EcoreUtil.copy(e1))
		the_and.setRight(EcoreUtil.copy(e2))
		return the_and
	}
	
	def static Expression get_disjunction(Expression e1, Expression e2)
	{
		val the_or = factory.createOr();
		the_or.setLeft(EcoreUtil.copy(e1))
		the_or.setRight(EcoreUtil.copy(e2))
		return the_or
	}
	
	
	def static List<Expression> multiply_clauses(List<Expression> left, List<Expression> right)
	{
		val product = list_product(left, right)
		
		val the_ors = product.stream().map([e | get_disjunction(e.getKey(),e.getValue())]).collect(Collectors.toList()); 
		
		return the_ors
	}
	
	def static boolean is_atom(EObject formula)
	{
		return is_var(formula) || is_truth_symbol(formula)
	}
	def static boolean is_var(EObject formula)
	{
		return ((formula as Expression).id !== null)	
	}
	
	def static boolean is_truth_symbol(EObject formula)
	{
		return ((formula as Expression).^val !== null)	
	}	
	
	def static get_id(EObject formula)
	{
		return (formula as Expression).id	
	}
	
	def static get_val(EObject formula)
	{
		return (formula as Expression).^val
	}
	
	def static Expression formula_to_equivalent_CNF(EObject formule)
	{
		/*
		println("Treating formula")
		pretty_print(formule)
		println()
		*/
		
		switch formule
		{
			case formule instanceof Formula :
			{	
				throw new Error("gneh")
			} 
			case formule instanceof Quantifier :
			{				
				throw new Error("gneh2")
			}
			case formule instanceof BiImpl :
			{	
				val the_biimpl = (formule as BiImpl)
						
				val the_and = factory.createAnd();
				val the_impl_1 = factory.createImpl()
				val the_impl_2 = factory.createImpl()
						
				
					
				val the_biimpl_left = EcoreUtil.copy(the_biimpl.left)
				val the_biimpl_right = EcoreUtil.copy(the_biimpl.right)
				
				
				the_impl_1.setLeft(the_biimpl.left)
				the_impl_1.setRight(the_biimpl.right)
				
				the_impl_2.setLeft ( the_biimpl_right )
				the_impl_2.setRight( the_biimpl_left  )
						
				the_and.setLeft(the_impl_1)
				the_and.setRight(the_impl_2)
								
				return formula_to_equivalent_CNF(the_and)	
			} 
			case formule instanceof Impl : 
			{	
				/*
				print("(" )
				formula_to_equivalent_CNF( (formule as Impl).left )  
				print(" -> " )
				formula_to_equivalent_CNF((formule as Impl).right)
				print(")" )
				
				*/
				
				val the_impl = (formule as Impl)
						
				val the_or = factory.createOr();
				val the_not = factory.createNot()
						
				the_not.setExpression(the_impl.left)
					
				the_or.setLeft(the_not)
				the_or.setRight(the_impl.right)
						
				return formula_to_equivalent_CNF(the_or)	
				
			}			
		
			
			case formule instanceof Or : 
			{	
				
				print("Treating a OR. formula : ")
				pretty_print(formule)
				println()
				
				val the_or = (formule as Or)
				val the_or_left = formula_to_equivalent_CNF(the_or.left)
				val the_or_right = formula_to_equivalent_CNF(the_or.right)
				/*
				print("the of left : ")
				pretty_print(the_or_left)
				println()
				*/
				
				val clauses_of_left_part = get_clauses(the_or_left)
				val clauses_of_right_part = get_clauses(the_or_right)	
				
				val clauses_cartesian = multiply_clauses(clauses_of_left_part, clauses_of_right_part)
				
				val big_and = clauses_cartesian.reduce([x, y | get_conjunction(x, y)])
				
				return big_and
				
			} 
			case formule instanceof And : 
			{	
				val the_and = (formule as And)
						
				val the_new_and = factory.createAnd();
				the_new_and.setLeft( formula_to_equivalent_CNF(the_and.left) )
				the_new_and.setRight( formula_to_equivalent_CNF(the_and.right) )
					
				return the_new_and
				
			} 
			case formule instanceof Nand : 
			{	
				val the_nand = (formule as Nand)
				
				val the_and = factory.createAnd();
				val the_not = factory.createNot()
				
				the_and.setLeft(the_nand.left)
				the_and.setRight(the_nand.right)
				
				the_not.setExpression(the_and)
				
				return formula_to_equivalent_CNF(the_not)
			} 
			//case formule instanceof Primary : 
			//	println("oooo")
			case formule instanceof Not : 
			{	
				val expression = (formule as Not).expression
				
				switch expression
				{
					case expression instanceof Not : 
					{
						return formula_to_equivalent_CNF( (expression as Not).expression ) 
					} 
					case expression instanceof Nand : 
					{
						val the_nand = (expression as Nand)
						val the_and = factory.createAnd();
				
						the_and.setLeft(the_nand.left)
						the_and.setRight(the_nand.right)
								
						return formula_to_equivalent_CNF(the_and)
					}
					case expression instanceof And : 
					{
						val the_and = (expression as And)
						
						val the_or = factory.createOr();
						val the_not_left = factory.createNot();
						val the_not_right = factory.createNot();
						
						the_not_left.setExpression(the_and.left)
						the_not_right.setExpression(the_and.right)
						
						the_or.setLeft(the_not_left)
						the_or.setRight(the_not_right)
								
						return formula_to_equivalent_CNF(the_or)	
					} 
					case expression instanceof Or : 
					{
						val the_or = (expression as Or)
						
						val the_and = factory.createAnd();
						val the_not_left = factory.createNot();
						val the_not_right = factory.createNot();
						
						the_not_left.setExpression(the_or.left)
						the_not_right.setExpression(the_or.right)
						
						the_and.setLeft(the_not_left)
						the_and.setRight(the_not_right)
								
						return formula_to_equivalent_CNF(the_and)						
					}
					case expression instanceof Impl : 
					{
						val the_impl = (expression as Impl)
						
						val the_and = factory.createAnd();
						val the_not_right = factory.createNot();
						
						the_not_right.setExpression(the_impl.right)
						
						the_and.setLeft(the_impl.left)
						the_and.setRight(the_not_right)
								
						return formula_to_equivalent_CNF(the_and)	
					} 
					case expression instanceof BiImpl : 
					{
						val the_biimpl = (expression as BiImpl)
						
						val the_and = factory.createAnd();
						val the_impl_1 = factory.createImpl()
						val the_impl_2 = factory.createImpl()
						val the_not = factory.createNot()
						
						the_impl_1.setLeft(the_biimpl.left)
						the_impl_1.setRight(the_biimpl.right)
						
						the_impl_2.setLeft(the_biimpl.right)
						the_impl_2.setRight(the_biimpl.left)
						
						the_and.setLeft(the_impl_1)
						the_and.setRight(the_impl_2)
						
						the_not.setExpression(the_and)
								
						return formula_to_equivalent_CNF(the_not)	
					}
					default : 
					{
						if (is_var(expression))	
						{
							return (formule as Expression)
						}
						else if(is_truth_symbol(expression))
						{
							val truth_symbol = get_val(expression)
							if(truth_symbol.equals("true"))
							{

								return get_false()
							}
							else
							{
								return get_true()
							}
						}
					}	
				}
			} 
			default : 
			{
				/*
				if(is_var(formule))
				{
					print( (formule as Expression).id)
				}
				if(is_truth_symbol(formule))
				{
					print( (formule as Expression).^val)
				}
				*/
				return (formule as Expression)
			}			
			
		}
	}
	
	
	def  static tseytinisation(EObject formule)
	{
		return
			
	}
		
	
	
	static var counter = 0
	def static get_new_variable()
	{
		var da_variable = factory.createExpression()
		da_variable.setId( ("x" + counter.toString) )
		return da_variable
	}
	
	def static get_quantifiers(EObject ast) 
	{
		return (ast as Formula).quantifiers
	}
	
	def static get_formula(EObject ast)
	{
		return (ast as Formula).model
	}
	
	def static void pprint(EObject ast)
	{
		println(ast)
	}
	
	def static void pretty_print(EObject formule)
	{
		switch formule
		{
			case formule instanceof Formula :
			{	
				val quantifiers = (formule as Formula).quantifiers
				val model = (formule as Formula).model

				println("Formula :")				
				quantifiers.forEach[x | pretty_print(x)];
				pretty_print(model)
			} 
			case formule instanceof Quantifier :
			{				
				if ((formule as Quantifier).type == TypeQuantifier.FORALL)
				{	
					print("Forall ")
					pretty_print((formule as Quantifier).^var)
					println()
				}
				else if ((formule as Quantifier).type == TypeQuantifier.EXISTS) 
				{
					print("Exists ")
					pretty_print((formule as Quantifier).^var)
					println()
				}
				else
				{
					println("gneh")
				}
			}
			case formule instanceof BiImpl :
			{	
				print("(" )
				pretty_print( (formule as BiImpl).left )  
				print(" <-> " )
				pretty_print((formule as BiImpl).right)
				print(")" )
			} 
			case formule instanceof Impl : 
			{	
				print("(" )
				pretty_print( (formule as Impl).left )  
				print(" -> " )
				pretty_print((formule as Impl).right)
				print(")" )
			} 
			case formule instanceof Or : 
			{	
				print("(" )
				pretty_print( (formule as Or).left )  
				print(" OR " )
				pretty_print((formule as Or).right)
				print(")" )
			} 
			case formule instanceof And : 
			{	
				print("(" )
				pretty_print( (formule as And).left )  
				print(" AND " );
				pretty_print((formule as And).right)
				print(")" )
			} 
			case formule instanceof Nand : 
			{	
				print("(" )
				pretty_print( (formule as Nand).left )  
				print(" NAND " );
				pretty_print((formule as Nand).right)
				print(")" )
			} 
			//case formule instanceof Primary : 
			//	println("oooo")
			case formule instanceof Not : 
			{	
				print("(" )
				print("NOT " );
				pretty_print((formule as Not).expression)
				print(")" )
			} 
			default : 
			{
				if(is_var(formule))
				{
					print( (formule as Expression).id)
				}
				if(is_truth_symbol(formule))
				{
					print( (formule as Expression).^val)
				}
			}			
			
		}
	}
	
	
	
	///////////////////////////////////////////////////////////////////////////////////
	
	
	
	
	def static String prop_to_Qdimacs(EObject formula, List<Quantifier> quantifiers)
	{
		tab_symb.clear()
		
		
		populate_tab_symb(formula)
		
		return(
		"p cnf " + tab_symb.size() + " " + count_clauses(formula) + "\n"
		+
		(introduce_quantifiers(quantifiers))
		+
		write_clauses(formula) + " 0"
		)
		
	}
	
	
	def static introduce_quantifiers(List<Quantifier> quantifiers)
	{
		var string_quantifiers = ""
		for (q : quantifiers)
		{
			val variable_index = (tab_symb.indexOf(q.^var.id) + 1)
			val type_quantifier = q.type == TypeQuantifier.EXISTS ? "e" : "a"
			string_quantifiers += (type_quantifier + " " + variable_index + " 0 \n")
		} 
			
		return string_quantifiers
	}
	
	def static String prop_to_dimacs(EObject formule)
	{
		tab_symb.clear()
		populate_tab_symb(formule)
		
		"p cnf " + tab_symb.size() + " " + count_clauses(formule) + "\n"
		+
		write_clauses(formule) + " 0"
	}
	
	def static String write_clauses(EObject formule)
	{
		switch formule
		{
			case formule instanceof Or : 
			{	
				write_clauses( (formule as Or).left ) + " "
				+
				write_clauses((formule as Or).right)
			} 
			case formule instanceof And : 
			{	
				write_clauses((formule as And).left ) + " 0\n"
				+
				write_clauses((formule as And).right)
			}
			case formule instanceof Not : 
			{	
				"-" + write_clauses((formule as Not).expression)
			} 
			default : 
			{
				if(is_var(formule))
				{
					"" + (tab_symb.indexOf((formule as Expression).id) + 1)
				}
				else if (is_truth_symbol(formule))
				{
					if (  ((formule as Expression).^val).equals("false") )
					{
						//do nothing
						return ""
					}
					else
					{
						throw new Error("Unable to write true clauses in dimacs")
					}
				}
				else
				{
					throw new Error("Never !")
				}
			}
		}
	}
	
	static ArrayList<String> tab_symb = new ArrayList<String>();
	static def int populate_tab_symb(EObject formule)
	{
		switch formule
		{
			case formule instanceof Or : 
			{	
				populate_tab_symb( (formule as Or).left )
				+
				populate_tab_symb((formule as Or).right)
			} 
			case formule instanceof And : 
			{	
				populate_tab_symb( (formule as And).left )
				+
				populate_tab_symb((formule as And).right)
			}
			case formule instanceof Not : 
			{	
				populate_tab_symb((formule as Not).expression)
			} 
			default : 
			{
				if((formule as Expression).id !== null)
				{
					var id = (formule as Expression).id
					if(!tab_symb.contains(id))
					{
						tab_symb.add(id)
						1
					}
					else
					{
						0
					}
				}
				else
				{
					0
				}
			}
		}
	}
	
	def static int count_clauses(EObject formule)
	{
		if(formule instanceof And)
		{
			count_clauses((formule as And).right) + count_clauses((formule as And).left)
		}
		else
		{
			1
		}
	}
	
	/////////////////////////////////////////////////////////////////////////////
	
	
	def static Expression TQBF_to_SAT(List<Quantifier> quantifiers, Expression formula, String prefix)
	{
		if (quantifiers.size() == 0 )
		{
			return formula
		}
		else
		{
			val quantifier = quantifiers.get(0)
			val name_var = (quantifier as Quantifier).^var.id
			val new_quantifiers = quantifiers.subList(1, quantifiers.size());
			
			if ((quantifier as Quantifier).type == TypeQuantifier.FORALL)
			{
				val sat_formula_left = TQBF_to_SAT(new_quantifiers, EcoreUtil.copy(formula), prefix + "l")
				val sat_formula_right = TQBF_to_SAT(new_quantifiers, EcoreUtil.copy(formula), prefix + "r")
				val the_and = factory.createAnd()
				the_and.setLeft(propagateFalse(sat_formula_left, name_var))
				the_and.setRight(propagateTrue(sat_formula_right, name_var))
				return the_and
			}
			else if ((quantifier as Quantifier).type == TypeQuantifier.EXISTS)
			{
				val sat_formula = TQBF_to_SAT(new_quantifiers, formula, prefix)
				return propagateName(sat_formula, name_var, name_var + prefix)	
			}
			else
			{
				throw new Error("HEIN?")
			}	
			
		}
	}
	
	def static Expression propagateFalse(Expression formula, String var_name)
	{
		switch formula
		{	
			case formula instanceof Formula :
			{	
				throw new Error("gneh")
			} 
			case formula instanceof Quantifier :
			{				
				throw new Error("gneh2")
			}
			case formula instanceof BiImpl :
			{	
				val the_biimpl = (formula as BiImpl)
						
				the_biimpl.setLeft(propagateFalse(the_biimpl.left, var_name))
				the_biimpl.setRight(propagateFalse(the_biimpl.right, var_name))
								
				return the_biimpl
			} 
			case formula instanceof Impl : 
			{	
				val the_impl = (formula as Impl)
						
				the_impl.setLeft(propagateFalse(the_impl.left, var_name))
				the_impl.setRight(propagateFalse(the_impl.right, var_name))
								
				return the_impl					
			}			
			case formula instanceof Or : 
			{	
				val the_or = (formula as Or)
						
				the_or.setLeft(propagateFalse(the_or.left, var_name))
				the_or.setRight(propagateFalse(the_or.right, var_name))
								
				return the_or	
			} 
			case formula instanceof And : 
			{	
				val the_and = (formula as And)
						
				the_and.setLeft(propagateFalse(the_and.left, var_name))
				the_and.setRight(propagateFalse(the_and.right, var_name))
								
				return the_and
				
			} 
			case formula instanceof Nand : 
			{		
				val the_nand = (formula as Nand)
						
				the_nand.setLeft(propagateFalse(the_nand.left, var_name))
				the_nand.setRight(propagateFalse(the_nand.right, var_name))
								
				return the_nand
			} 
			case formula instanceof Not : 
			{	
				val the_not = (formula as Not)
						
				the_not.setExpression(propagateFalse(the_not.expression, var_name))
								
				return the_not
			} 
			default : 
			{
				if(is_var(formula))
				{
					if( ( (formula as Expression).id).equals(var_name) )
					{
						return get_false()
					}
				}
				if(is_truth_symbol(formula))
				{
				}
				return formula
			}			
		}
	}
	
	
	def static Expression propagateTrue(Expression formula, String var_name)
	{		
		switch formula
		{	
			case formula instanceof Formula :
			{	
				throw new Error("gneh")
			} 
			case formula instanceof Quantifier :
			{				
				throw new Error("gneh2")
			}
			case formula instanceof BiImpl :
			{	
				val the_biimpl = (formula as BiImpl)
						
				the_biimpl.setLeft(propagateTrue(the_biimpl.left, var_name))
				the_biimpl.setRight(propagateTrue(the_biimpl.right, var_name))
								
				return the_biimpl
			} 
			case formula instanceof Impl : 
			{	
				val the_impl = (formula as Impl)
						
				the_impl.setLeft(propagateTrue(the_impl.left, var_name))
				the_impl.setRight(propagateTrue(the_impl.right, var_name))
								
				return the_impl					
			}			
			case formula instanceof Or : 
			{	
				val the_or = (formula as Or)
						
				the_or.setLeft(propagateTrue(the_or.left, var_name))
				the_or.setRight(propagateTrue(the_or.right, var_name))
								
				return the_or	
			} 
			case formula instanceof And : 
			{	
				val the_and = (formula as And)
						
				the_and.setLeft(propagateTrue(the_and.left, var_name))
				the_and.setRight(propagateTrue(the_and.right, var_name))
								
				return the_and
				
			} 
			case formula instanceof Nand : 
			{		
				val the_nand = (formula as Nand)
						
				the_nand.setLeft(propagateTrue(the_nand.left, var_name))
				the_nand.setRight(propagateTrue(the_nand.right, var_name))
								
				return the_nand
			} 
			case formula instanceof Not : 
			{	
				val the_not = (formula as Not)
						
				the_not.setExpression(propagateTrue(the_not.expression, var_name))
								
				return the_not
			} 
			default : 
			{
				
				if(is_var(formula))
				{
					if( ( (formula as Expression).id).equals(var_name) )
					{
						println("oooh")
						return get_true()
					}
				}
				if(is_truth_symbol(formula))
				{
				}
				return formula
			}			
		}
	
	}
	
	def static Expression propagateName(Expression formula, String name_var, String new_name_var)
	{
		switch formula
		{	
			case formula instanceof Formula :
			{	
				throw new Error("gneh")
			} 
			case formula instanceof Quantifier :
			{				
				throw new Error("gneh2")
			}
			case formula instanceof BiImpl :
			{	
				
				val the_biimpl = (formula as BiImpl)
				val left = propagateName(the_biimpl.left, name_var, new_name_var)
				the_biimpl.setLeft(left)
				the_biimpl.setRight(propagateName(the_biimpl.right, name_var, new_name_var))
								
				return the_biimpl
			} 
			case formula instanceof Impl : 
			{	
				val the_impl = (formula as Impl)
						
				the_impl.setLeft(propagateName(the_impl.left, name_var, new_name_var))
				the_impl.setRight(propagateName(the_impl.right, name_var, new_name_var))
								
				return the_impl					
			}			
			case formula instanceof Or : 
			{	
				val the_or = (formula as Or)
						
				the_or.setLeft(propagateName(the_or.left, name_var, new_name_var))
				the_or.setRight(propagateName(the_or.right, name_var, new_name_var))
								
				return the_or	
			} 
			case formula instanceof And : 
			{	
				val the_and = (formula as And)
						
				the_and.setLeft(propagateName(the_and.left, name_var, new_name_var))
				the_and.setRight(propagateName(the_and.right, name_var, new_name_var))
								
				return the_and
				
			} 
			case formula instanceof Nand : 
			{		
				val the_nand = (formula as Nand)
						
				the_nand.setLeft(propagateName(the_nand.left, name_var, new_name_var))
				the_nand.setRight(propagateName(the_nand.right, name_var, new_name_var))
								
				return the_nand
			} 
			case formula instanceof Not : 
			{	
				val the_not = (formula as Not)
						
				the_not.setExpression(propagateName(the_not.expression, name_var, new_name_var))
								
				return the_not
			} 
			default : 
			{
				if(is_var(formula))
				{
					if( ( (formula as Expression).id).equals(name_var) )
					{
						(formula as Expression).id = new_name_var				
					}
				}
				if(is_truth_symbol(formula))
				{
				}
				return formula
			}			
		}
	}
	
	
	////////////////////////////////////////////////////////////////////////
	
	
	
	
	def static Expression remove_truth_values_from_cnf(Expression formula)
	{
		var clauses = get_clauses(formula)
		println("Here are all the clauses : ")

		for (c : clauses)
		{
			pretty_print(c)
			println()
		}		
		
		val clauses_2 = remove_true_clauses(clauses)
		
		
		
		val new_formula = clauses_2.reduce([x, y | get_conjunction(x,y)])
		return new_formula
	}
	
	
	def static List<Expression> remove_true_clauses(List<Expression> clauses)
	{
		val clauses_after = clauses.stream().filter([ x | (!is_trivial(x))]).collect(Collectors.toList())
		
		println("size before : " + (clauses.size()) + "   size after : " + (clauses_after.size))
		return clauses_after
	}
	
	def static boolean is_trivial(Expression clause)
	{
		switch clause
		{
			case clause instanceof Or : 
			{
				val left = (clause as Or).left
				val right = (clause as Or).right
				
				return (is_trivial(left)  || is_trivial(right))
			}
			case clause instanceof Not : 
			{
				var expr = (clause as Not).expression
				return (is_truth_symbol(expr) && ((expr as Expression).^val).equals("false"))
			}
			default : 
			{
				if(is_var(clause))
				{
					return false
				}	
				else if (is_truth_symbol(clause)) 
				{

					if (((clause as Expression).^val).equals("true") )
					{
						return true
					}
					return false
				}
				else
				{
					println("hein ? ")
					pretty_print(clause)
					println()
					throw new Error("Uh ?")
				}
			}
		}
	}

}