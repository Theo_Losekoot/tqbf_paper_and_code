package org.xtext.spa.qbf.gen;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.xtext.resource.XtextResource;
import org.eclipse.xtext.resource.XtextResourceSet;
import org.xtext.spa.qbf.QbfStandaloneSetup;
import org.xtext.spa.qbf.qbf.Formula;

import com.google.inject.Injector;

public class Qbfgenerator
{

	public static void main(String[] args)  throws IOException
	{
		maiin();
	}
		
		static void maiin() throws IOException
		{
			//val ast = parseHelper.parse('''
			//		A ^ (B v C) ^ (~A)
			//		sat4j-java
			//''')
			
			String input = "/home/jacob/Desktop/SAT-DSLmorphic/org.xtext.spa.qbf/src/formula.qbf";
			
			String text = new String(Files.readAllBytes(Paths.get(input)), StandardCharsets.UTF_8);


			
			System.out.println("text read : ");
			System.out.println(text);
			System.out.println();
			
	/*
			new StandaloneSetup().setPlatformUri("../");
			var injector = new QbfStandaloneSetup().createInjectorAndDoEMFRegistration();
			var resourceSet = injector.getInstance(XtextResourceSet.class);
			resourceSet.addLoadOption(XtextResource.OPTION_RESOLVE_ALL, Boolean.TRUE);
			var resource = resourceSet.getResource(
	    	URI.createURI("platform:/resource/org.xtext.example.mydsl/src/example.mydsl"), true);
			var model = (Model) resource.getContents().get(0);
	*/

			new org.eclipse.emf.mwe.utils.StandaloneSetup().setPlatformUri("../");
			Injector injector = new QbfStandaloneSetup().createInjectorAndDoEMFRegistration();
			XtextResourceSet resourceSet = injector.getInstance(XtextResourceSet.class);
			resourceSet.addLoadOption(XtextResource.OPTION_RESOLVE_ALL, Boolean.TRUE);
			Resource resource = resourceSet.getResource(
	    	URI.createURI(input), true);
			Formula model = (Formula) resource.getContents().get(0);
			
			

			//val ast = parseHelper.parse(text);

					
			// dimacs_formula = read_entry(ast)

			
			
			System.out.println("dimcas fomula : \n")	;	
			System.out.println(model);
			System.out.println();
			


			String filename_of_formula = "output.cnf";
			FileWriter fileWriter = new FileWriter(new File(filename_of_formula));
			fileWriter.write("la formule");
			fileWriter.close();
			
			
		
			
		}
		
		/*
		
		def String read_entry(Formula ast)
		{
			return prop_to_dimacs( (ast.model as EObject))
		}
		
		def String prop_to_dimacs(EObject formule)
		{
			tab_symb.clear()
			populate_tab_symb(formule)
			
			"p cnf " + tab_symb.size() + " " + count_clauses(formule) + "\n"
			+
			write_clauses(formule) + " 0"
		}
		
		def String write_clauses(EObject formule)
		{
			switch formule
			{
				case formule instanceof Or : 
				{	
					write_clauses( (formule as Or).left ) + " "
					+
					write_clauses((formule as Or).right)
				} 
				case formule instanceof And : 
				{	
					write_clauses((formule as And).left ) + " 0\n"
					+
					write_clauses((formule as And).right)
				}
				case formule instanceof Not : 
				{	
					"-" + write_clauses((formule as Not).expression)
				} 
				default : 
				{
					if((formule as Expression).id !== null)
					{
						"" + (tab_symb.indexOf((formule as Expression).id) + 1)
					}
					else
					{
						"ERROR"
					}
				}
			}
		}
		
		static ArrayList<String> tab_symb = new ArrayList<String>();
		static def int populate_tab_symb(EObject formule)
		{
			switch formule
			{
				case formule instanceof Or : 
				{	
					populate_tab_symb( (formule as Or).left )
					+
					populate_tab_symb((formule as Or).right)
				} 
				case formule instanceof And : 
				{	
					populate_tab_symb( (formule as And).left )
					+
					populate_tab_symb((formule as And).right)
				}
				case formule instanceof Not : 
				{	
					populate_tab_symb((formule as Not).expression)
				} 
				default : 
				{
					if((formule as Expression).id !== null)
					{
						var id = (formule as Expression).id
						if(!tab_symb.contains(id))
						{
							tab_symb.add(id)
							1
						}
						else
						{
							0
						}
					}
					else
					{
						0
					}
				}
			}
		}
		
		def int count_clauses(EObject formule)
		{
			if(formule instanceof And)
			{
				count_clauses((formule as And).right) + count_clauses((formule as And).left)
			}
			else
			{
				1
			}
		}
		
		def void pretty_print(EObject formule)
		{
			switch formule
			{
				case formule instanceof BiImpl :
				{	
					print("(" )
					pretty_print( (formule as BiImpl).left )  
					print(" <-> " )
					pretty_print((formule as BiImpl).right)
					print(")" )
				} 
				case formule instanceof Impl : 
				{	
					print("(" )
					pretty_print( (formule as Impl).left )  
					print(" -> " )
					pretty_print((formule as Impl).right)
					print(")" )
				} 
				case formule instanceof Or : 
				{	
					print("(" )
					pretty_print( (formule as Or).left )  
					print(" OR " )
					pretty_print((formule as Or).right)
					print(")" )
				} 
				case formule instanceof And : 
				{	
					print("(" )
					pretty_print( (formule as And).left )  
					print(" AND " );
					pretty_print((formule as And).right)
					print(")" )
				} 
				case formule instanceof Nand : 
				{	
					print("(" )
					pretty_print( (formule as Nand).left )  
					print(" NAND " );
					pretty_print((formule as Nand).right)
					print(")" )
				} 
				//case formule instanceof Primary : 
				//	println("oooo")
				case formule instanceof Not : 
				{	
					print("(" )
					print("NOT " );
					pretty_print((formule as Not).expression)
					print(")" )
				} 
				default : 
				{
					if((formule as Expression).id !== null)
					{
						print( (formule as Expression).id)
					}
					if((formule as Expression).^val !== null)
					{
						print( (formule as Expression).^val)
					}
				}
				//case formule instanceof Var : 
				//	println("oooo")
				//case formule instanceof Const : 
				//	println("oooo")
				
				
			}
		}
		*/
	}
