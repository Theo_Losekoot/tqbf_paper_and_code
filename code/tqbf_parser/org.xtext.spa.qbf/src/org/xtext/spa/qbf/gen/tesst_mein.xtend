package org.xtext.spa.qbf.gen

import org.eclipse.emf.ecore.EObject
import org.xtext.spa.qbf.qbf.And
import org.xtext.spa.qbf.qbf.BiImpl
import org.xtext.spa.qbf.qbf.Expression
import org.xtext.spa.qbf.qbf.Formula
import org.xtext.spa.qbf.qbf.Impl
import org.xtext.spa.qbf.qbf.Nand
import org.xtext.spa.qbf.qbf.Not
import org.xtext.spa.qbf.qbf.Or

class tesst_mein 
{
	def static void main(String[] args) 
	{
		val test = new testt();
		test.main(args);
		
		val ast = test.parse(args);
		pretty_print(ast)
	}
	
	
	
		def static void pretty_print(EObject formule)
		{
			print("heyo\n")
			switch formule
			{
				case formule instanceof Formula :
				{	
					println("FORMULAA")
				} 
				case formule instanceof BiImpl :
				{	
					print("(" )
					pretty_print( (formule as BiImpl).left )  
					print(" <-> " )
					pretty_print((formule as BiImpl).right)
					print(")" )
				} 
				case formule instanceof Impl : 
				{	
					print("(" )
					pretty_print( (formule as Impl).left )  
					print(" -> " )
					pretty_print((formule as Impl).right)
					print(")" )
				} 
				case formule instanceof Or : 
				{	
					print("(" )
					pretty_print( (formule as Or).left )  
					print(" OR " )
					pretty_print((formule as Or).right)
					print(")" )
				} 
				case formule instanceof And : 
				{	
					print("(" )
					pretty_print( (formule as And).left )  
					print(" AND " );
					pretty_print((formule as And).right)
					print(")" )
				} 
				case formule instanceof Nand : 
				{	
					print("(" )
					pretty_print( (formule as Nand).left )  
					print(" NAND " );
					pretty_print((formule as Nand).right)
					print(")" )
				} 
				//case formule instanceof Primary : 
				//	println("oooo")
				case formule instanceof Not : 
				{	
					print("(" )
					print("NOT " );
					pretty_print((formule as Not).expression)
					print(")" )
				} 
				default : 
				{
					println("formule: ")
					println(formule)
					if((formule as Expression).id !== null)
					{
						print( (formule as Expression).id)
					}
					if((formule as Expression).^val !== null)
					{
						print( (formule as Expression).^val)
					}
				}			
				
			}
		}
}