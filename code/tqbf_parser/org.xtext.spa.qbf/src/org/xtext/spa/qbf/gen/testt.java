package org.xtext.spa.qbf.gen;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.IParseResult;
import org.eclipse.xtext.parser.IParser;
import org.xtext.spa.qbf.QbfStandaloneSetup;

import com.google.inject.Inject;
import com.google.inject.Injector;

public class testt
{
	 
	public void main(String[] args)  throws IOException
	{
		
		XtextParser();
		String input = "/home/jacob/Desktop/SAT-DSLmorphic/org.xtext.spa.qbf/src/formula.qbf";
		
		String text = new String(Files.readAllBytes(Paths.get(input)), StandardCharsets.UTF_8);


        IParseResult result = parser.parse(new StringReader(text));
        
        System.out.println(text);
        System.out.println(result);

		
	}
	
	public EObject parse(String[] args) throws IOException
	{	
		XtextParser();
		String input = "/home/jacob/Desktop/SAT-DSLmorphic/org.xtext.spa.qbf/src/formula.qbf";
		
		String text = new String(Files.readAllBytes(Paths.get(input)), StandardCharsets.UTF_8);


        IParseResult result = parser.parse(new StringReader(text));
        
        System.out.println(text);
        System.out.println(result);
        
        return result.getRootASTElement();
	}
	
    @Inject
    private IParser parser;
 
    public void XtextParser()
    {
        setupParser();
    }
 
    private void setupParser() {
        Injector injector = new QbfStandaloneSetup().createInjectorAndDoEMFRegistration();
        injector.injectMembers(this);
    }
 
    /**
     * Parses data provided by an input reader using Xtext and returns the root node of the resulting object tree.
     * @param reader Input reader
     * @return root object node
     * @throws IOException when errors occur during the parsing process
     */
    public EObject parse(Reader reader) throws IOException
    {
        IParseResult result = parser.parse(reader);
        if(result.hasSyntaxErrors())
        {
            throw new IOException("Provided input contains syntax errors.");
        }
        return result.getRootASTElement();
    }
}