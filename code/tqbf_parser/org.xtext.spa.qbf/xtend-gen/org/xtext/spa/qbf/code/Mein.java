package org.xtext.spa.qbf.code;

import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.xtext.xbase.lib.Conversions;
import org.eclipse.xtext.xbase.lib.Exceptions;
import org.eclipse.xtext.xbase.lib.InputOutput;
import org.xtext.spa.qbf.code.Reader;
import org.xtext.spa.qbf.code.Transformations;
import org.xtext.spa.qbf.qbf.Expression;
import org.xtext.spa.qbf.qbf.Formula;
import org.xtext.spa.qbf.qbf.Quantifier;

@SuppressWarnings("all")
public class Mein {
  public static void main(final String[] args) {
    try {
      int _size = ((List<String>)Conversions.doWrapArray(args)).size();
      boolean _lessThan = (_size < 3);
      if (_lessThan) {
        throw new Error("Usage : java -jar tqbfparser <input file> <output_file> <method (\"qdimacs\" or \"dimacs\")>");
      }
      final String filename = args[0];
      InputOutput.<String>println(("filename: " + filename));
      final Reader read = new Reader();
      read.XtextParser();
      FileReader _fileReader = new FileReader(filename);
      EObject _parse = read.parse(_fileReader);
      final Formula ast = ((Formula) _parse);
      Transformations.pretty_print(ast);
      InputOutput.println();
      final String output = args[1];
      final String method = args[2];
      boolean _equals = method.equals("qdimacs");
      if (_equals) {
        Mein.get_qdimacs(ast, output);
      } else {
        boolean _equals_1 = method.equals("dimacs");
        if (_equals_1) {
          Mein.naive_convert_to_sat(ast, output);
        } else {
          InputOutput.<String>print("invalid method name");
        }
      }
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  public static void get_qdimacs(final Formula ast, final String filename_dimacs) {
    final EList<Quantifier> quantifiers = Transformations.get_quantifiers(ast);
    final Expression formula = Transformations.get_formula(ast);
    InputOutput.<String>println("formula : ");
    Transformations.pretty_print(formula);
    InputOutput.println();
    final Expression sat_formula_cnf = Transformations.formula_to_equivalent_CNF(EcoreUtil.<Expression>copy(formula));
    final String qsat_formula_cnf_dimacs = Transformations.prop_to_Qdimacs(sat_formula_cnf, quantifiers);
    InputOutput.<String>println("qbf -> sat formula -> cnf -> dimacs : ");
    InputOutput.<String>println(qsat_formula_cnf_dimacs);
    InputOutput.println();
    Mein.save(filename_dimacs, qsat_formula_cnf_dimacs);
    InputOutput.println();
  }
  
  public static void naive_convert_to_sat(final Formula ast, final String filename_dimacs) {
    final EList<Quantifier> quantifiers = Transformations.get_quantifiers(ast);
    final Expression formula = Transformations.get_formula(ast);
    InputOutput.<String>println("formula : ");
    Transformations.pretty_print(formula);
    InputOutput.println();
    final Expression sat_formula = Transformations.TQBF_to_SAT(quantifiers, EcoreUtil.<Expression>copy(formula), "#");
    final Expression sat_formula_cnf = Transformations.formula_to_equivalent_CNF(EcoreUtil.<Expression>copy(sat_formula));
    final Expression sat_formula_cnf_nonT = Transformations.remove_truth_values_from_cnf(sat_formula_cnf);
    final String sat_formula_cnf_dimacs = Transformations.prop_to_dimacs(EcoreUtil.<Expression>copy(sat_formula_cnf_nonT));
    InputOutput.<String>println("qbf -> sat formula -> cnf -> dimacs : ");
    InputOutput.<String>println(sat_formula_cnf_dimacs);
    InputOutput.println();
    Mein.save(filename_dimacs, sat_formula_cnf_dimacs);
    InputOutput.println();
  }
  
  public static String save(final String filename, final String content) {
    String _xtrycatchfinallyexpression = null;
    try {
      FileWriter _fileWriter = new FileWriter(filename);
      final BufferedWriter writer = new BufferedWriter(_fileWriter);
      writer.write((content + "\n"));
      writer.close();
    } catch (final Throwable _t) {
      if (_t instanceof IOException) {
        final IOException e = (IOException)_t;
        _xtrycatchfinallyexpression = InputOutput.<String>print(((("error writing in " + filename) + " : ") + e));
      } else {
        throw Exceptions.sneakyThrow(_t);
      }
    }
    return _xtrycatchfinallyexpression;
  }
}
