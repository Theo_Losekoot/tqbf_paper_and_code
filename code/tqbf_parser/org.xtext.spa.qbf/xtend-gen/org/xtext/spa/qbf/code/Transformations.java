package org.xtext.spa.qbf.code;

import com.google.common.base.Objects;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.xtext.xbase.lib.Functions.Function2;
import org.eclipse.xtext.xbase.lib.InputOutput;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.eclipse.xtext.xbase.lib.Pair;
import org.xtext.spa.qbf.qbf.And;
import org.xtext.spa.qbf.qbf.BiImpl;
import org.xtext.spa.qbf.qbf.Expression;
import org.xtext.spa.qbf.qbf.Formula;
import org.xtext.spa.qbf.qbf.Impl;
import org.xtext.spa.qbf.qbf.Nand;
import org.xtext.spa.qbf.qbf.Not;
import org.xtext.spa.qbf.qbf.Or;
import org.xtext.spa.qbf.qbf.QbfFactory;
import org.xtext.spa.qbf.qbf.Quantifier;
import org.xtext.spa.qbf.qbf.TypeQuantifier;

@SuppressWarnings("all")
public class Transformations {
  private static final QbfFactory factory = QbfFactory.eINSTANCE;
  
  public static List<Expression> get_clauses(final Expression expression) {
    if ((expression instanceof Or)) {
      final ArrayList<Expression> list = new ArrayList<Expression>();
      list.add(expression);
      return list;
    } else {
      if ((expression instanceof And)) {
        List<Expression> l1 = Transformations.get_clauses(((And) expression).getLeft());
        final List<Expression> l2 = Transformations.get_clauses(((And) expression).getRight());
        l1.addAll(l2);
        return l1;
      } else {
        if ((expression instanceof Not)) {
          final ArrayList<Expression> list_1 = new ArrayList<Expression>();
          list_1.add(expression);
          return list_1;
        } else {
          boolean _is_atom = Transformations.is_atom(expression);
          if (_is_atom) {
            final ArrayList<Expression> list_2 = new ArrayList<Expression>();
            list_2.add(expression);
            return list_2;
          } else {
            InputOutput.<String>print("formula : ");
            Transformations.pretty_print(expression);
            InputOutput.println();
            throw new Error("gaaah");
          }
        }
      }
    }
  }
  
  private static <TypeA extends Object, TypeB extends Object> List<Pair<TypeA, TypeB>> list_product(final List<TypeA> l1, final List<TypeB> l2) {
    ArrayList<Pair<TypeA, TypeB>> list = new ArrayList<Pair<TypeA, TypeB>>();
    for (final TypeA e1 : l1) {
      for (final TypeB e2 : l2) {
        Pair<TypeA, TypeB> _pair = new Pair<TypeA, TypeB>(e1, e2);
        list.add(_pair);
      }
    }
    return list;
  }
  
  public static Expression get_true() {
    final Expression truu = Transformations.factory.createExpression();
    truu.setVal("true");
    return truu;
  }
  
  public static Expression get_GNAA() {
    final Expression truu = Transformations.factory.createExpression();
    truu.setVal("GNAAA");
    return truu;
  }
  
  public static Expression get_false() {
    final Expression falss = Transformations.factory.createExpression();
    falss.setVal("false");
    return falss;
  }
  
  public static Expression get_conjunction(final Expression e1, final Expression e2) {
    final And the_and = Transformations.factory.createAnd();
    the_and.setLeft(EcoreUtil.<Expression>copy(e1));
    the_and.setRight(EcoreUtil.<Expression>copy(e2));
    return the_and;
  }
  
  public static Expression get_disjunction(final Expression e1, final Expression e2) {
    final Or the_or = Transformations.factory.createOr();
    the_or.setLeft(EcoreUtil.<Expression>copy(e1));
    the_or.setRight(EcoreUtil.<Expression>copy(e2));
    return the_or;
  }
  
  public static List<Expression> multiply_clauses(final List<Expression> left, final List<Expression> right) {
    final List<Pair<Expression, Expression>> product = Transformations.<Expression, Expression>list_product(left, right);
    final Function<Pair<Expression, Expression>, Expression> _function = (Pair<Expression, Expression> e) -> {
      return Transformations.get_disjunction(e.getKey(), e.getValue());
    };
    final List<Expression> the_ors = product.stream().<Expression>map(_function).collect(Collectors.<Expression>toList());
    return the_ors;
  }
  
  public static boolean is_atom(final EObject formula) {
    return (Transformations.is_var(formula) || Transformations.is_truth_symbol(formula));
  }
  
  public static boolean is_var(final EObject formula) {
    String _id = ((Expression) formula).getId();
    return (_id != null);
  }
  
  public static boolean is_truth_symbol(final EObject formula) {
    String _val = ((Expression) formula).getVal();
    return (_val != null);
  }
  
  public static String get_id(final EObject formula) {
    return ((Expression) formula).getId();
  }
  
  public static String get_val(final EObject formula) {
    return ((Expression) formula).getVal();
  }
  
  public static Expression formula_to_equivalent_CNF(final EObject formule) {
    boolean _matched = false;
    if ((formule instanceof Formula)) {
      _matched=true;
      throw new Error("gneh");
    }
    if (!_matched) {
      if ((formule instanceof Quantifier)) {
        _matched=true;
        throw new Error("gneh2");
      }
    }
    if (!_matched) {
      if ((formule instanceof BiImpl)) {
        _matched=true;
        final BiImpl the_biimpl = ((BiImpl) formule);
        final And the_and = Transformations.factory.createAnd();
        final Impl the_impl_1 = Transformations.factory.createImpl();
        final Impl the_impl_2 = Transformations.factory.createImpl();
        final Expression the_biimpl_left = EcoreUtil.<Expression>copy(the_biimpl.getLeft());
        final Expression the_biimpl_right = EcoreUtil.<Expression>copy(the_biimpl.getRight());
        the_impl_1.setLeft(the_biimpl.getLeft());
        the_impl_1.setRight(the_biimpl.getRight());
        the_impl_2.setLeft(the_biimpl_right);
        the_impl_2.setRight(the_biimpl_left);
        the_and.setLeft(the_impl_1);
        the_and.setRight(the_impl_2);
        return Transformations.formula_to_equivalent_CNF(the_and);
      }
    }
    if (!_matched) {
      if ((formule instanceof Impl)) {
        _matched=true;
        final Impl the_impl = ((Impl) formule);
        final Or the_or = Transformations.factory.createOr();
        final Not the_not = Transformations.factory.createNot();
        the_not.setExpression(the_impl.getLeft());
        the_or.setLeft(the_not);
        the_or.setRight(the_impl.getRight());
        return Transformations.formula_to_equivalent_CNF(the_or);
      }
    }
    if (!_matched) {
      if ((formule instanceof Or)) {
        _matched=true;
        InputOutput.<String>print("Treating a OR. formula : ");
        Transformations.pretty_print(formule);
        InputOutput.println();
        final Or the_or_1 = ((Or) formule);
        final Expression the_or_left = Transformations.formula_to_equivalent_CNF(the_or_1.getLeft());
        final Expression the_or_right = Transformations.formula_to_equivalent_CNF(the_or_1.getRight());
        final List<Expression> clauses_of_left_part = Transformations.get_clauses(the_or_left);
        final List<Expression> clauses_of_right_part = Transformations.get_clauses(the_or_right);
        final List<Expression> clauses_cartesian = Transformations.multiply_clauses(clauses_of_left_part, clauses_of_right_part);
        final Function2<Expression, Expression, Expression> _function = (Expression x, Expression y) -> {
          return Transformations.get_conjunction(x, y);
        };
        final Expression big_and = IterableExtensions.<Expression>reduce(clauses_cartesian, _function);
        return big_and;
      }
    }
    if (!_matched) {
      if ((formule instanceof And)) {
        _matched=true;
        final And the_and_1 = ((And) formule);
        final And the_new_and = Transformations.factory.createAnd();
        the_new_and.setLeft(Transformations.formula_to_equivalent_CNF(the_and_1.getLeft()));
        the_new_and.setRight(Transformations.formula_to_equivalent_CNF(the_and_1.getRight()));
        return the_new_and;
      }
    }
    if (!_matched) {
      if ((formule instanceof Nand)) {
        _matched=true;
        final Nand the_nand = ((Nand) formule);
        final And the_and_2 = Transformations.factory.createAnd();
        final Not the_not_1 = Transformations.factory.createNot();
        the_and_2.setLeft(the_nand.getLeft());
        the_and_2.setRight(the_nand.getRight());
        the_not_1.setExpression(the_and_2);
        return Transformations.formula_to_equivalent_CNF(the_not_1);
      }
    }
    if (!_matched) {
      if ((formule instanceof Not)) {
        _matched=true;
        final Expression expression = ((Not) formule).getExpression();
        boolean _matched_1 = false;
        if ((expression instanceof Not)) {
          _matched_1=true;
          return Transformations.formula_to_equivalent_CNF(((Not) expression).getExpression());
        }
        if (!_matched_1) {
          if ((expression instanceof Nand)) {
            _matched_1=true;
            final Nand the_nand_1 = ((Nand) expression);
            final And the_and_3 = Transformations.factory.createAnd();
            the_and_3.setLeft(the_nand_1.getLeft());
            the_and_3.setRight(the_nand_1.getRight());
            return Transformations.formula_to_equivalent_CNF(the_and_3);
          }
        }
        if (!_matched_1) {
          if ((expression instanceof And)) {
            _matched_1=true;
            final And the_and_4 = ((And) expression);
            final Or the_or_2 = Transformations.factory.createOr();
            final Not the_not_left = Transformations.factory.createNot();
            final Not the_not_right = Transformations.factory.createNot();
            the_not_left.setExpression(the_and_4.getLeft());
            the_not_right.setExpression(the_and_4.getRight());
            the_or_2.setLeft(the_not_left);
            the_or_2.setRight(the_not_right);
            return Transformations.formula_to_equivalent_CNF(the_or_2);
          }
        }
        if (!_matched_1) {
          if ((expression instanceof Or)) {
            _matched_1=true;
            final Or the_or_3 = ((Or) expression);
            final And the_and_5 = Transformations.factory.createAnd();
            final Not the_not_left_1 = Transformations.factory.createNot();
            final Not the_not_right_1 = Transformations.factory.createNot();
            the_not_left_1.setExpression(the_or_3.getLeft());
            the_not_right_1.setExpression(the_or_3.getRight());
            the_and_5.setLeft(the_not_left_1);
            the_and_5.setRight(the_not_right_1);
            return Transformations.formula_to_equivalent_CNF(the_and_5);
          }
        }
        if (!_matched_1) {
          if ((expression instanceof Impl)) {
            _matched_1=true;
            final Impl the_impl_3 = ((Impl) expression);
            final And the_and_6 = Transformations.factory.createAnd();
            final Not the_not_right_2 = Transformations.factory.createNot();
            the_not_right_2.setExpression(the_impl_3.getRight());
            the_and_6.setLeft(the_impl_3.getLeft());
            the_and_6.setRight(the_not_right_2);
            return Transformations.formula_to_equivalent_CNF(the_and_6);
          }
        }
        if (!_matched_1) {
          if ((expression instanceof BiImpl)) {
            _matched_1=true;
            final BiImpl the_biimpl_1 = ((BiImpl) expression);
            final And the_and_7 = Transformations.factory.createAnd();
            final Impl the_impl_1_1 = Transformations.factory.createImpl();
            final Impl the_impl_2_1 = Transformations.factory.createImpl();
            final Not the_not_2 = Transformations.factory.createNot();
            the_impl_1_1.setLeft(the_biimpl_1.getLeft());
            the_impl_1_1.setRight(the_biimpl_1.getRight());
            the_impl_2_1.setLeft(the_biimpl_1.getRight());
            the_impl_2_1.setRight(the_biimpl_1.getLeft());
            the_and_7.setLeft(the_impl_1_1);
            the_and_7.setRight(the_impl_2_1);
            the_not_2.setExpression(the_and_7);
            return Transformations.formula_to_equivalent_CNF(the_not_2);
          }
        }
        boolean _is_var = Transformations.is_var(expression);
        if (_is_var) {
          return ((Expression) formule);
        } else {
          boolean _is_truth_symbol = Transformations.is_truth_symbol(expression);
          if (_is_truth_symbol) {
            final String truth_symbol = Transformations.get_val(expression);
            boolean _equals = truth_symbol.equals("true");
            if (_equals) {
              return Transformations.get_false();
            } else {
              return Transformations.get_true();
            }
          }
        }
      }
    }
    if (!_matched) {
      return ((Expression) formule);
    }
    return null;
  }
  
  public static void tseytinisation(final EObject formule) {
    return;
  }
  
  private static int counter = 0;
  
  public static Expression get_new_variable() {
    Expression da_variable = Transformations.factory.createExpression();
    String _string = Integer.valueOf(Transformations.counter).toString();
    String _plus = ("x" + _string);
    da_variable.setId(_plus);
    return da_variable;
  }
  
  public static EList<Quantifier> get_quantifiers(final EObject ast) {
    return ((Formula) ast).getQuantifiers();
  }
  
  public static Expression get_formula(final EObject ast) {
    return ((Formula) ast).getModel();
  }
  
  public static void pprint(final EObject ast) {
    InputOutput.<EObject>println(ast);
  }
  
  public static void pretty_print(final EObject formule) {
    boolean _matched = false;
    if ((formule instanceof Formula)) {
      _matched=true;
      final EList<Quantifier> quantifiers = ((Formula) formule).getQuantifiers();
      final Expression model = ((Formula) formule).getModel();
      InputOutput.<String>println("Formula :");
      final Consumer<Quantifier> _function = (Quantifier x) -> {
        Transformations.pretty_print(x);
      };
      quantifiers.forEach(_function);
      Transformations.pretty_print(model);
    }
    if (!_matched) {
      if ((formule instanceof Quantifier)) {
        _matched=true;
        TypeQuantifier _type = ((Quantifier) formule).getType();
        boolean _equals = Objects.equal(_type, TypeQuantifier.FORALL);
        if (_equals) {
          InputOutput.<String>print("Forall ");
          Transformations.pretty_print(((Quantifier) formule).getVar());
          InputOutput.println();
        } else {
          TypeQuantifier _type_1 = ((Quantifier) formule).getType();
          boolean _equals_1 = Objects.equal(_type_1, TypeQuantifier.EXISTS);
          if (_equals_1) {
            InputOutput.<String>print("Exists ");
            Transformations.pretty_print(((Quantifier) formule).getVar());
            InputOutput.println();
          } else {
            InputOutput.<String>println("gneh");
          }
        }
      }
    }
    if (!_matched) {
      if ((formule instanceof BiImpl)) {
        _matched=true;
        InputOutput.<String>print("(");
        Transformations.pretty_print(((BiImpl) formule).getLeft());
        InputOutput.<String>print(" <-> ");
        Transformations.pretty_print(((BiImpl) formule).getRight());
        InputOutput.<String>print(")");
      }
    }
    if (!_matched) {
      if ((formule instanceof Impl)) {
        _matched=true;
        InputOutput.<String>print("(");
        Transformations.pretty_print(((Impl) formule).getLeft());
        InputOutput.<String>print(" -> ");
        Transformations.pretty_print(((Impl) formule).getRight());
        InputOutput.<String>print(")");
      }
    }
    if (!_matched) {
      if ((formule instanceof Or)) {
        _matched=true;
        InputOutput.<String>print("(");
        Transformations.pretty_print(((Or) formule).getLeft());
        InputOutput.<String>print(" OR ");
        Transformations.pretty_print(((Or) formule).getRight());
        InputOutput.<String>print(")");
      }
    }
    if (!_matched) {
      if ((formule instanceof And)) {
        _matched=true;
        InputOutput.<String>print("(");
        Transformations.pretty_print(((And) formule).getLeft());
        InputOutput.<String>print(" AND ");
        Transformations.pretty_print(((And) formule).getRight());
        InputOutput.<String>print(")");
      }
    }
    if (!_matched) {
      if ((formule instanceof Nand)) {
        _matched=true;
        InputOutput.<String>print("(");
        Transformations.pretty_print(((Nand) formule).getLeft());
        InputOutput.<String>print(" NAND ");
        Transformations.pretty_print(((Nand) formule).getRight());
        InputOutput.<String>print(")");
      }
    }
    if (!_matched) {
      if ((formule instanceof Not)) {
        _matched=true;
        InputOutput.<String>print("(");
        InputOutput.<String>print("NOT ");
        Transformations.pretty_print(((Not) formule).getExpression());
        InputOutput.<String>print(")");
      }
    }
    if (!_matched) {
      {
        boolean _is_var = Transformations.is_var(formule);
        if (_is_var) {
          InputOutput.<String>print(((Expression) formule).getId());
        }
        boolean _is_truth_symbol = Transformations.is_truth_symbol(formule);
        if (_is_truth_symbol) {
          InputOutput.<String>print(((Expression) formule).getVal());
        }
      }
    }
  }
  
  public static String prop_to_Qdimacs(final EObject formula, final List<Quantifier> quantifiers) {
    Transformations.tab_symb.clear();
    Transformations.populate_tab_symb(formula);
    int _size = Transformations.tab_symb.size();
    String _plus = ("p cnf " + Integer.valueOf(_size));
    String _plus_1 = (_plus + " ");
    int _count_clauses = Transformations.count_clauses(formula);
    String _plus_2 = (_plus_1 + Integer.valueOf(_count_clauses));
    String _plus_3 = (_plus_2 + "\n");
    String _introduce_quantifiers = Transformations.introduce_quantifiers(quantifiers);
    String _plus_4 = (_plus_3 + _introduce_quantifiers);
    String _write_clauses = Transformations.write_clauses(formula);
    String _plus_5 = (_plus_4 + _write_clauses);
    return (_plus_5 + " 0");
  }
  
  public static String introduce_quantifiers(final List<Quantifier> quantifiers) {
    String string_quantifiers = "";
    for (final Quantifier q : quantifiers) {
      {
        int _indexOf = Transformations.tab_symb.indexOf(q.getVar().getId());
        final int variable_index = (_indexOf + 1);
        String _xifexpression = null;
        TypeQuantifier _type = q.getType();
        boolean _equals = Objects.equal(_type, TypeQuantifier.EXISTS);
        if (_equals) {
          _xifexpression = "e";
        } else {
          _xifexpression = "a";
        }
        final String type_quantifier = _xifexpression;
        String _string_quantifiers = string_quantifiers;
        string_quantifiers = (_string_quantifiers + (((type_quantifier + " ") + Integer.valueOf(variable_index)) + " 0 \n"));
      }
    }
    return string_quantifiers;
  }
  
  public static String prop_to_dimacs(final EObject formule) {
    String _xblockexpression = null;
    {
      Transformations.tab_symb.clear();
      Transformations.populate_tab_symb(formule);
      int _size = Transformations.tab_symb.size();
      String _plus = ("p cnf " + Integer.valueOf(_size));
      String _plus_1 = (_plus + " ");
      int _count_clauses = Transformations.count_clauses(formule);
      String _plus_2 = (_plus_1 + Integer.valueOf(_count_clauses));
      String _plus_3 = (_plus_2 + "\n");
      String _write_clauses = Transformations.write_clauses(formule);
      String _plus_4 = (_plus_3 + _write_clauses);
      _xblockexpression = (_plus_4 + " 0");
    }
    return _xblockexpression;
  }
  
  public static String write_clauses(final EObject formule) {
    String _switchResult = null;
    boolean _matched = false;
    if ((formule instanceof Or)) {
      _matched=true;
      String _write_clauses = Transformations.write_clauses(((Or) formule).getLeft());
      String _plus = (_write_clauses + " ");
      String _write_clauses_1 = Transformations.write_clauses(((Or) formule).getRight());
      _switchResult = (_plus + _write_clauses_1);
    }
    if (!_matched) {
      if ((formule instanceof And)) {
        _matched=true;
        String _write_clauses_2 = Transformations.write_clauses(((And) formule).getLeft());
        String _plus_1 = (_write_clauses_2 + " 0\n");
        String _write_clauses_3 = Transformations.write_clauses(((And) formule).getRight());
        _switchResult = (_plus_1 + _write_clauses_3);
      }
    }
    if (!_matched) {
      if ((formule instanceof Not)) {
        _matched=true;
        String _write_clauses_4 = Transformations.write_clauses(((Not) formule).getExpression());
        _switchResult = ("-" + _write_clauses_4);
      }
    }
    if (!_matched) {
      String _xifexpression = null;
      boolean _is_var = Transformations.is_var(formule);
      if (_is_var) {
        int _indexOf = Transformations.tab_symb.indexOf(((Expression) formule).getId());
        int _plus_2 = (_indexOf + 1);
        _xifexpression = ("" + Integer.valueOf(_plus_2));
      } else {
        boolean _is_truth_symbol = Transformations.is_truth_symbol(formule);
        if (_is_truth_symbol) {
          boolean _equals = ((Expression) formule).getVal().equals("false");
          if (_equals) {
            return "";
          } else {
            throw new Error("Unable to write true clauses in dimacs");
          }
        } else {
          throw new Error("Never !");
        }
      }
      _switchResult = _xifexpression;
    }
    return _switchResult;
  }
  
  private static ArrayList<String> tab_symb = new ArrayList<String>();
  
  public static int populate_tab_symb(final EObject formule) {
    int _switchResult = (int) 0;
    boolean _matched = false;
    if ((formule instanceof Or)) {
      _matched=true;
      int _populate_tab_symb = Transformations.populate_tab_symb(((Or) formule).getLeft());
      int _populate_tab_symb_1 = Transformations.populate_tab_symb(((Or) formule).getRight());
      _switchResult = (_populate_tab_symb + _populate_tab_symb_1);
    }
    if (!_matched) {
      if ((formule instanceof And)) {
        _matched=true;
        int _populate_tab_symb_2 = Transformations.populate_tab_symb(((And) formule).getLeft());
        int _populate_tab_symb_3 = Transformations.populate_tab_symb(((And) formule).getRight());
        _switchResult = (_populate_tab_symb_2 + _populate_tab_symb_3);
      }
    }
    if (!_matched) {
      if ((formule instanceof Not)) {
        _matched=true;
        _switchResult = Transformations.populate_tab_symb(((Not) formule).getExpression());
      }
    }
    if (!_matched) {
      int _xifexpression = (int) 0;
      String _id = ((Expression) formule).getId();
      boolean _tripleNotEquals = (_id != null);
      if (_tripleNotEquals) {
        int _xblockexpression = (int) 0;
        {
          String id = ((Expression) formule).getId();
          int _xifexpression_1 = (int) 0;
          boolean _contains = Transformations.tab_symb.contains(id);
          boolean _not = (!_contains);
          if (_not) {
            int _xblockexpression_1 = (int) 0;
            {
              Transformations.tab_symb.add(id);
              _xblockexpression_1 = 1;
            }
            _xifexpression_1 = _xblockexpression_1;
          } else {
            _xifexpression_1 = 0;
          }
          _xblockexpression = _xifexpression_1;
        }
        _xifexpression = _xblockexpression;
      } else {
        _xifexpression = 0;
      }
      _switchResult = _xifexpression;
    }
    return _switchResult;
  }
  
  public static int count_clauses(final EObject formule) {
    int _xifexpression = (int) 0;
    if ((formule instanceof And)) {
      int _count_clauses = Transformations.count_clauses(((And) formule).getRight());
      int _count_clauses_1 = Transformations.count_clauses(((And) formule).getLeft());
      _xifexpression = (_count_clauses + _count_clauses_1);
    } else {
      _xifexpression = 1;
    }
    return _xifexpression;
  }
  
  public static Expression TQBF_to_SAT(final List<Quantifier> quantifiers, final Expression formula, final String prefix) {
    int _size = quantifiers.size();
    boolean _equals = (_size == 0);
    if (_equals) {
      return formula;
    } else {
      final Quantifier quantifier = quantifiers.get(0);
      final String name_var = ((Quantifier) quantifier).getVar().getId();
      final List<Quantifier> new_quantifiers = quantifiers.subList(1, quantifiers.size());
      TypeQuantifier _type = ((Quantifier) quantifier).getType();
      boolean _equals_1 = Objects.equal(_type, TypeQuantifier.FORALL);
      if (_equals_1) {
        final Expression sat_formula_left = Transformations.TQBF_to_SAT(new_quantifiers, EcoreUtil.<Expression>copy(formula), (prefix + "l"));
        final Expression sat_formula_right = Transformations.TQBF_to_SAT(new_quantifiers, EcoreUtil.<Expression>copy(formula), (prefix + "r"));
        final And the_and = Transformations.factory.createAnd();
        the_and.setLeft(Transformations.propagateFalse(sat_formula_left, name_var));
        the_and.setRight(Transformations.propagateTrue(sat_formula_right, name_var));
        return the_and;
      } else {
        TypeQuantifier _type_1 = ((Quantifier) quantifier).getType();
        boolean _equals_2 = Objects.equal(_type_1, TypeQuantifier.EXISTS);
        if (_equals_2) {
          final Expression sat_formula = Transformations.TQBF_to_SAT(new_quantifiers, formula, prefix);
          return Transformations.propagateName(sat_formula, name_var, (name_var + prefix));
        } else {
          throw new Error("HEIN?");
        }
      }
    }
  }
  
  public static Expression propagateFalse(final Expression formula, final String var_name) {
    boolean _matched = false;
    if ((formula instanceof Formula)) {
      _matched=true;
      throw new Error("gneh");
    }
    if (!_matched) {
      if ((formula instanceof Quantifier)) {
        _matched=true;
        throw new Error("gneh2");
      }
    }
    if (!_matched) {
      if ((formula instanceof BiImpl)) {
        _matched=true;
        final BiImpl the_biimpl = ((BiImpl) formula);
        the_biimpl.setLeft(Transformations.propagateFalse(the_biimpl.getLeft(), var_name));
        the_biimpl.setRight(Transformations.propagateFalse(the_biimpl.getRight(), var_name));
        return the_biimpl;
      }
    }
    if (!_matched) {
      if ((formula instanceof Impl)) {
        _matched=true;
        final Impl the_impl = ((Impl) formula);
        the_impl.setLeft(Transformations.propagateFalse(the_impl.getLeft(), var_name));
        the_impl.setRight(Transformations.propagateFalse(the_impl.getRight(), var_name));
        return the_impl;
      }
    }
    if (!_matched) {
      if ((formula instanceof Or)) {
        _matched=true;
        final Or the_or = ((Or) formula);
        the_or.setLeft(Transformations.propagateFalse(the_or.getLeft(), var_name));
        the_or.setRight(Transformations.propagateFalse(the_or.getRight(), var_name));
        return the_or;
      }
    }
    if (!_matched) {
      if ((formula instanceof And)) {
        _matched=true;
        final And the_and = ((And) formula);
        the_and.setLeft(Transformations.propagateFalse(the_and.getLeft(), var_name));
        the_and.setRight(Transformations.propagateFalse(the_and.getRight(), var_name));
        return the_and;
      }
    }
    if (!_matched) {
      if ((formula instanceof Nand)) {
        _matched=true;
        final Nand the_nand = ((Nand) formula);
        the_nand.setLeft(Transformations.propagateFalse(the_nand.getLeft(), var_name));
        the_nand.setRight(Transformations.propagateFalse(the_nand.getRight(), var_name));
        return the_nand;
      }
    }
    if (!_matched) {
      if ((formula instanceof Not)) {
        _matched=true;
        final Not the_not = ((Not) formula);
        the_not.setExpression(Transformations.propagateFalse(the_not.getExpression(), var_name));
        return the_not;
      }
    }
    {
      boolean _is_var = Transformations.is_var(formula);
      if (_is_var) {
        boolean _equals = ((Expression) formula).getId().equals(var_name);
        if (_equals) {
          return Transformations.get_false();
        }
      }
      boolean _is_truth_symbol = Transformations.is_truth_symbol(formula);
      if (_is_truth_symbol) {
      }
      return formula;
    }
  }
  
  public static Expression propagateTrue(final Expression formula, final String var_name) {
    boolean _matched = false;
    if ((formula instanceof Formula)) {
      _matched=true;
      throw new Error("gneh");
    }
    if (!_matched) {
      if ((formula instanceof Quantifier)) {
        _matched=true;
        throw new Error("gneh2");
      }
    }
    if (!_matched) {
      if ((formula instanceof BiImpl)) {
        _matched=true;
        final BiImpl the_biimpl = ((BiImpl) formula);
        the_biimpl.setLeft(Transformations.propagateTrue(the_biimpl.getLeft(), var_name));
        the_biimpl.setRight(Transformations.propagateTrue(the_biimpl.getRight(), var_name));
        return the_biimpl;
      }
    }
    if (!_matched) {
      if ((formula instanceof Impl)) {
        _matched=true;
        final Impl the_impl = ((Impl) formula);
        the_impl.setLeft(Transformations.propagateTrue(the_impl.getLeft(), var_name));
        the_impl.setRight(Transformations.propagateTrue(the_impl.getRight(), var_name));
        return the_impl;
      }
    }
    if (!_matched) {
      if ((formula instanceof Or)) {
        _matched=true;
        final Or the_or = ((Or) formula);
        the_or.setLeft(Transformations.propagateTrue(the_or.getLeft(), var_name));
        the_or.setRight(Transformations.propagateTrue(the_or.getRight(), var_name));
        return the_or;
      }
    }
    if (!_matched) {
      if ((formula instanceof And)) {
        _matched=true;
        final And the_and = ((And) formula);
        the_and.setLeft(Transformations.propagateTrue(the_and.getLeft(), var_name));
        the_and.setRight(Transformations.propagateTrue(the_and.getRight(), var_name));
        return the_and;
      }
    }
    if (!_matched) {
      if ((formula instanceof Nand)) {
        _matched=true;
        final Nand the_nand = ((Nand) formula);
        the_nand.setLeft(Transformations.propagateTrue(the_nand.getLeft(), var_name));
        the_nand.setRight(Transformations.propagateTrue(the_nand.getRight(), var_name));
        return the_nand;
      }
    }
    if (!_matched) {
      if ((formula instanceof Not)) {
        _matched=true;
        final Not the_not = ((Not) formula);
        the_not.setExpression(Transformations.propagateTrue(the_not.getExpression(), var_name));
        return the_not;
      }
    }
    {
      boolean _is_var = Transformations.is_var(formula);
      if (_is_var) {
        boolean _equals = ((Expression) formula).getId().equals(var_name);
        if (_equals) {
          InputOutput.<String>println("oooh");
          return Transformations.get_true();
        }
      }
      boolean _is_truth_symbol = Transformations.is_truth_symbol(formula);
      if (_is_truth_symbol) {
      }
      return formula;
    }
  }
  
  public static Expression propagateName(final Expression formula, final String name_var, final String new_name_var) {
    boolean _matched = false;
    if ((formula instanceof Formula)) {
      _matched=true;
      throw new Error("gneh");
    }
    if (!_matched) {
      if ((formula instanceof Quantifier)) {
        _matched=true;
        throw new Error("gneh2");
      }
    }
    if (!_matched) {
      if ((formula instanceof BiImpl)) {
        _matched=true;
        final BiImpl the_biimpl = ((BiImpl) formula);
        final Expression left = Transformations.propagateName(the_biimpl.getLeft(), name_var, new_name_var);
        the_biimpl.setLeft(left);
        the_biimpl.setRight(Transformations.propagateName(the_biimpl.getRight(), name_var, new_name_var));
        return the_biimpl;
      }
    }
    if (!_matched) {
      if ((formula instanceof Impl)) {
        _matched=true;
        final Impl the_impl = ((Impl) formula);
        the_impl.setLeft(Transformations.propagateName(the_impl.getLeft(), name_var, new_name_var));
        the_impl.setRight(Transformations.propagateName(the_impl.getRight(), name_var, new_name_var));
        return the_impl;
      }
    }
    if (!_matched) {
      if ((formula instanceof Or)) {
        _matched=true;
        final Or the_or = ((Or) formula);
        the_or.setLeft(Transformations.propagateName(the_or.getLeft(), name_var, new_name_var));
        the_or.setRight(Transformations.propagateName(the_or.getRight(), name_var, new_name_var));
        return the_or;
      }
    }
    if (!_matched) {
      if ((formula instanceof And)) {
        _matched=true;
        final And the_and = ((And) formula);
        the_and.setLeft(Transformations.propagateName(the_and.getLeft(), name_var, new_name_var));
        the_and.setRight(Transformations.propagateName(the_and.getRight(), name_var, new_name_var));
        return the_and;
      }
    }
    if (!_matched) {
      if ((formula instanceof Nand)) {
        _matched=true;
        final Nand the_nand = ((Nand) formula);
        the_nand.setLeft(Transformations.propagateName(the_nand.getLeft(), name_var, new_name_var));
        the_nand.setRight(Transformations.propagateName(the_nand.getRight(), name_var, new_name_var));
        return the_nand;
      }
    }
    if (!_matched) {
      if ((formula instanceof Not)) {
        _matched=true;
        final Not the_not = ((Not) formula);
        the_not.setExpression(Transformations.propagateName(the_not.getExpression(), name_var, new_name_var));
        return the_not;
      }
    }
    {
      boolean _is_var = Transformations.is_var(formula);
      if (_is_var) {
        boolean _equals = ((Expression) formula).getId().equals(name_var);
        if (_equals) {
          ((Expression) formula).setId(new_name_var);
        }
      }
      boolean _is_truth_symbol = Transformations.is_truth_symbol(formula);
      if (_is_truth_symbol) {
      }
      return formula;
    }
  }
  
  public static Expression remove_truth_values_from_cnf(final Expression formula) {
    List<Expression> clauses = Transformations.get_clauses(formula);
    InputOutput.<String>println("Here are all the clauses : ");
    for (final Expression c : clauses) {
      {
        Transformations.pretty_print(c);
        InputOutput.println();
      }
    }
    final List<Expression> clauses_2 = Transformations.remove_true_clauses(clauses);
    final Function2<Expression, Expression, Expression> _function = (Expression x, Expression y) -> {
      return Transformations.get_conjunction(x, y);
    };
    final Expression new_formula = IterableExtensions.<Expression>reduce(clauses_2, _function);
    return new_formula;
  }
  
  public static List<Expression> remove_true_clauses(final List<Expression> clauses) {
    final Predicate<Expression> _function = (Expression x) -> {
      boolean _is_trivial = Transformations.is_trivial(x);
      return (!_is_trivial);
    };
    final List<Expression> clauses_after = clauses.stream().filter(_function).collect(Collectors.<Expression>toList());
    int _size = clauses.size();
    String _plus = ("size before : " + Integer.valueOf(_size));
    String _plus_1 = (_plus + "   size after : ");
    int _size_1 = clauses_after.size();
    String _plus_2 = (_plus_1 + Integer.valueOf(_size_1));
    InputOutput.<String>println(_plus_2);
    return clauses_after;
  }
  
  public static boolean is_trivial(final Expression clause) {
    boolean _matched = false;
    if ((clause instanceof Or)) {
      _matched=true;
      final Expression left = ((Or) clause).getLeft();
      final Expression right = ((Or) clause).getRight();
      return (Transformations.is_trivial(left) || Transformations.is_trivial(right));
    }
    if (!_matched) {
      if ((clause instanceof Not)) {
        _matched=true;
        Expression expr = ((Not) clause).getExpression();
        return (Transformations.is_truth_symbol(expr) && ((Expression) expr).getVal().equals("false"));
      }
    }
    boolean _is_var = Transformations.is_var(clause);
    if (_is_var) {
      return false;
    } else {
      boolean _is_truth_symbol = Transformations.is_truth_symbol(clause);
      if (_is_truth_symbol) {
        boolean _equals = ((Expression) clause).getVal().equals("true");
        if (_equals) {
          return true;
        }
        return false;
      } else {
        InputOutput.<String>println("hein ? ");
        Transformations.pretty_print(clause);
        InputOutput.println();
        throw new Error("Uh ?");
      }
    }
  }
}
