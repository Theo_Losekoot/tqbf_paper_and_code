#include <iostream>
#include <fstream>
#include <vector>
#include <queue>
#include <string>
#include <sstream>
#include <algorithm>
#include <map>
#include <cstring>



using namespace std;


void remove_all_those_quantifiers( queue<pair<char, vector<int>>> &quantifiers, vector<vector<pair<int, string>>> &clauses);
void print_clauses(vector<vector<pair<int, string>>> &clauses);
void remove_quantifier_forall(vector<int> &variables_quantified, vector<vector<pair<int, string>>> &clauses);
void remove_quantifier_forall_one_var(int var, vector<vector<pair<int, string>>> &clauses);
void add_clauses_where_var_is_there_but_now_without_var(int var, vector<pair<int, string>> &clause, vector<vector<pair<int, string>>> &new_clauses, char modifier, string &precise_current_var);
void remove_those_from_vect(vector<int> &to_delete, vector<vector<pair<int, string>>> &clauses);
void remove_quantifier_exists(vector<int> &variables_quantified, vector<vector<pair<int, string>>> &clauses);
void remove_quantifier_exists_one_var(int var, vector<vector<pair<int, string>>> &clauses);
bool is_value_negative_in_vect(int val, vector<pair<int, string>> &vect, string &precise_current_var);
bool is_value_positive_in_vect(int val, vector<pair<int, string>> &vect, string &precise_current_var);
bool is_value_or_complement_in_vect(int val, vector<pair<int, string>> &vect);
void compute_disjunction_between_these_sets(vector<vector<pair<int, string>>> &clauses_1, vector<vector<pair<int, string>>> &clauses_2, vector<vector<pair<int, string>>> &new_clauses);
void merge_two_clauses(vector<pair<int, string>> &clause_1, vector<pair<int, string>> &clause_2, vector<pair<int, string>> &new_clause);
void parse_entry(vector<string> &lines, queue<pair<char, vector<int>>> &quantifiers, vector<vector<pair<int, string>>> &clauses);
vector<string> split(string line);
vector<int> vect_to_int(vector<string> &vect);
void map_all_variables(map<string, int> &new_ids, vector<vector<pair<int, string>>> clauses);
int lookup(string key, map<string, int> &new_ids);
string get_positive_version_of_identifier(string &identifier);
void print_clause(vector<pair<int, string>> &clause);


template<typename K, typename V> void print_map(map<K,V> const &m);

int main(int argc, char**argv)
{
	if (argc < 3)
	{
		cout << "usage : ./exec <input_file> <output_file>" << endl;
		return 1;
	}
	string input_file = argv[1];
	string output_file = argv[2];
	vector<string> lines;
	string line = "";
	std::ifstream infile(input_file);
    while (std::getline(infile, line))
    {
		if (line[0] != 'c') 
        	lines.push_back(line);
    }
/*
	cout << "vector : " << endl;
	
	for (string liine : lines)
	{
		cout << liine << endl;
	}
*/
	string header = lines.at(0);
	lines.erase(lines.begin(), lines.begin()+1);

	queue<pair<char, vector<int> > > quantifiers;
	vector<vector<pair<int, string>>> clauses;
	parse_entry(lines, quantifiers, clauses);

	/*
	cout << "quantifiers : " << endl;
	while(!quantifiers.empty())
	{
		pair<char, vector<int>> quantifier_and_variables = quantifiers.front(); quantifiers.pop();
		char quantifier = quantifier_and_variables.first;
		vector<int> variables = quantifier_and_variables.second;
		cout << "quantified with " << quantifier << " : ";
		for( int var : variables)
		{
			cout << var << " @ ";
		}		
		cout << endl;
	}

	cout << "clauses : " << endl;
	for(vector<int> clause : clauses)
	{
		for(int v : clause)
		{
			cout << v << " @ ";
		}
		cout << endl;
	}
	*/
	remove_all_those_quantifiers(quantifiers, clauses);
	
	/*
	cout << "clauses : " << endl;
	for(vector<int> clause : clauses)
	{
		for(int v : clause)
		{
			cout << v << " @ ";
		}
		cout << endl;
	}
	*/
	map<string, int> new_ids;
	map_all_variables(new_ids, clauses);

/*
	cout << "map : " << endl;
	print_map(new_ids);
*/
	int nb_var = new_ids.size();
	int nb_clauses = clauses.size();
	ofstream outfile;
   	outfile.open(output_file);
	outfile << "p cnf " << nb_var << " " << nb_clauses;
	for(vector<pair<int, string>> clause : clauses)
	{
		outfile << endl;
		for(pair<int, string> v : clause)
		{
			int var = lookup(v.second, new_ids);
			//cout << "key : " << v.second << "   var : " << var  << endl;
			outfile << lookup(v.second, new_ids)  << " ";
		}
		outfile << "0";
	}
	return 0;
}

int lookup(string key, map<string, int> &new_ids)
{
	string positive_key = get_positive_version_of_identifier(key);
	bool is_positive = true;
	if (key[0] == '-')
	{
 		is_positive = false;
	}
	
	if(is_positive)
		return new_ids[positive_key];
	else
		return -new_ids[positive_key];
}

void map_all_variables(map<string, int> &new_ids, vector<vector<pair<int, string>>> clauses)
{
	int free_identifier = 1;
	for(vector<pair<int, string>> clause : clauses)
	{
		for(pair<int, string> var : clause)
		{
			string identifier = var.second;
			string positive_identifier = get_positive_version_of_identifier(identifier);
			if ( new_ids.find(positive_identifier) == new_ids.end() )
 			{
				new_ids.insert(make_pair(positive_identifier, free_identifier));
				free_identifier++;
			}
			else
			{
				//do nothing
			}
		}
	}
}




void remove_all_those_quantifiers( queue<pair<char, vector<int>>> &quantifiers, vector<vector<pair<int, string>>> &clauses)
{
	int i = 0;
	int max = quantifiers.size();
	while(!quantifiers.empty())
	{
		if (quantifiers.front().first == 'e' && quantifiers.size() == 1)
			break;
		pair<char, vector<int>> quantifier_and_variables = quantifiers.front(); quantifiers.pop();
		char quantifier = quantifier_and_variables.first;
		vector<int> variables_quantified = quantifier_and_variables.second;
		cout  << "eliminating wave " << i++ << "/" << max << " of quantifiers (" << quantifier << ")" << endl;

		//cout << "treating quantifier " << quantifier << endl;

		if(quantifier == 'a')
		{
			remove_quantifier_forall(variables_quantified, clauses);
		}
		else if(quantifier == 'e')
		{
			remove_quantifier_exists(variables_quantified, clauses);
		}
	}
}

void print_clauses(vector<vector<pair<int, string>>> &clauses)
{
	cout << "clauses : " << endl;
	for(vector<pair<int, string>> clause : clauses)
	{
		for(pair<int, string> v : clause)
		{
			cout << v.first << "(" << v.second << ")" << " @ ";
		}
		cout << endl;
	}
}


void print_clause(vector<pair<int, string>> &clause)
{
	cout << "clause : " << endl;
	for(pair<int, string> v : clause)
	{
		cout << v.first << "(" << v.second << ")" << " @ ";
	}
	cout << endl;
}

string get_positive_version_of_identifier(string &identifier)
{
	string positive_identifier = "";
	for(int i = 0 ; i < identifier.size() ; i++)
		positive_identifier.push_back(identifier[i]);
	if (positive_identifier[0] == '-')
		positive_identifier.erase(positive_identifier.begin()); 
	return positive_identifier;
}

void remove_quantifier_forall(vector<int> &variables_quantified, vector<vector<pair<int, string>>> &clauses)
{
	for(int var : variables_quantified)
	{
		remove_quantifier_forall_one_var(var, clauses);
	}
}

void remove_quantifier_forall_one_var(int var, vector<vector<pair<int, string>>> &clauses)
{
	bool another_round = true;
	while(another_round)
	{
		vector<int> to_delete;
		vector<vector<pair<int, string>>> new_clauses_when_false;
		vector<vector<pair<int, string>>> new_clauses_when_true;

		string precise_current_var = "";

		for(int i=0; i<clauses.size(); i++)
		{
			vector<pair<int, string>> clause = clauses[i];
			if(is_value_positive_in_vect(var, clause, precise_current_var))
			{
				to_delete.push_back(i);
				add_clauses_where_var_is_there_but_now_without_var(var, clause, new_clauses_when_true, 'T', precise_current_var);
			}
			else if(is_value_negative_in_vect(var, clause, precise_current_var))
			{
				to_delete.push_back(i);
				add_clauses_where_var_is_there_but_now_without_var(-var, clause, new_clauses_when_false, 'B', precise_current_var);
			}
		}
		remove_those_from_vect(to_delete, clauses); 
		clauses.insert(clauses.end(), new_clauses_when_true.begin(), new_clauses_when_true.end());
		clauses.insert(clauses.end(), new_clauses_when_false.begin(), new_clauses_when_false.end());

		another_round = (new_clauses_when_true.size()>0 || new_clauses_when_false.size()>0);
	}
}

void add_clauses_where_var_is_there_but_now_without_var(int var, vector<pair<int, string>> &clause, vector<vector<pair<int, string>>> &new_clauses, char modifier, string &precise_current_var)
{
	vector<pair<int, string>> vector_with_var_to_false;
	for(pair<int, string> value : clause)
	{
		if(value.first != var || (precise_current_var.compare(get_positive_version_of_identifier(value.second)) != 0) )
		{
			pair<int, string> new_var = make_pair(value.first, value.second + modifier);
			vector_with_var_to_false.push_back(new_var);
		}
	}
	new_clauses.push_back(vector_with_var_to_false);
}

/*
bool same_var(pair<int, string> v1, string v2)
{
	return abs(v1) == abs(v2);
}	
*/

void remove_those_from_vect(vector<int> &to_delete, vector<vector<pair<int, string>>> &clauses)
{
	int index_shift = 0;
	for(int index : to_delete)
	{
		clauses.erase(clauses.begin() + index - index_shift);
		index_shift++;
	}
}

void remove_quantifier_exists(vector<int> &variables_quantified, vector<vector<pair<int, string>>> &clauses)
{
	for(int var : variables_quantified)
	{
		remove_quantifier_exists_one_var(var, clauses);
	}
}

void remove_quantifier_exists_one_var(int var, vector<vector<pair<int, string>>> &clauses)
{
	bool another_round = true;
	while(another_round)
	{
		//cout << "var: " << var << endl;
		string precise_current_var = "";
	
		vector<int> to_delete;

		vector<vector<pair<int, string>>> new_clauses_var_true;
		vector<vector<pair<int, string>>> new_clauses_var_false;

		for(int i=0; i<clauses.size(); i++)
		{
			//cout << "clauses size : " << clauses.size() << endl;
			//print_clauses(clauses);
			vector<pair<int, string>> clause = clauses[i];
			if(is_value_positive_in_vect(var, clause, precise_current_var))
			{
				to_delete.push_back(i);
				add_clauses_where_var_is_there_but_now_without_var(var, clause, new_clauses_var_true, 'T', precise_current_var);
			}
			else if(is_value_negative_in_vect(var, clause, precise_current_var))
			{
				to_delete.push_back(i);
				add_clauses_where_var_is_there_but_now_without_var(-var, clause, new_clauses_var_false, 'B', precise_current_var);
			}
		}

		remove_those_from_vect(to_delete, clauses); 
		//computing the disjunction between these two sets
		vector<vector<pair<int, string>>> new_clauses;
		compute_disjunction_between_these_sets(new_clauses_var_true, new_clauses_var_false, new_clauses);
		clauses.insert(clauses.end(), new_clauses.begin(), new_clauses.end());	
	
		another_round = (new_clauses_var_true.size()>0 || new_clauses_var_false.size()>0);
	}
}

bool is_value_negative_in_vect(int val, vector<pair<int, string>> &vect, string &precise_current_var)
{
	return is_value_positive_in_vect(-val, vect, precise_current_var);
}

bool is_value_positive_in_vect(int val, vector<pair<int, string>> &vect, string &precise_current_var)
{
	
	for(pair<int, string> element : vect)
	{
		if(element.first == val)
		{
			if (precise_current_var.compare("") == 0)
 			{
				precise_current_var = get_positive_version_of_identifier(element.second);
				return true;
			}
			else if(get_positive_version_of_identifier(element.second).compare(precise_current_var) == 0 )
			{
				return true;
			}
		}
	}
	return false;
}

/*
bool is_value_or_complement_in_vect(int val, vector<pair<int, string>> &vect)
{
	return (is_value_positive_in_vect(val, vect) || is_value_negative_in_vect(val, vect));
}
*/

void compute_disjunction_between_these_sets(vector<vector<pair<int, string>>> &clauses_1, vector<vector<pair<int, string>>> &clauses_2, vector<vector<pair<int, string>>> &new_clauses)
{
	for(vector<pair<int, string>> clause_1 : clauses_1)
	{
		for(vector<pair<int, string>> clause_2 : clauses_2)
		{
			vector<pair<int, string>> new_clause;
 			merge_two_clauses(clause_1, clause_2, new_clause);
			new_clauses.push_back(new_clause);
			
		}
	}
}

void merge_two_clauses(vector<pair<int, string>> &clause_1, vector<pair<int, string>> &clause_2, vector<pair<int, string>> &new_clause)
{
	new_clause.insert(new_clause.begin(), clause_1.begin(), clause_1.end());
	new_clause.insert(new_clause.end(), clause_2.begin(), clause_2.end());
	print_clause(new_clause);
}

void parse_entry(vector<string> &lines, queue<pair<char, vector<int>>> &quantifiers, vector<vector<pair<int, string>>> &clauses)
{
	int i = 0;
	while (i < lines.size())
	{
		char quantifier = lines[i][0];
		if(quantifier == 'a' || quantifier == 'e')
		{
			string line = lines[i];
			vector<string> parts = split(line);
			parts.erase(parts.begin());
			parts.erase(parts.end());
			vector<int> variables_id = vect_to_int(parts);
			quantifiers.push(make_pair(quantifier, variables_id));
		}
		else
		{
			string line = lines[i];
			if (line.size() == 0)
				break;
			vector<string> clause_str = split(line);
			clause_str.erase(clause_str.end());	
			//vector<string> clause = clause_str//vect_to_int(clause_str);
			vector<int> clause_id = vect_to_int(clause_str);
			vector<pair<int,string>> clause;
			for(int i =0; i< clause_id.size(); i++)
			{
				clause.push_back(make_pair(clause_id[i], clause_str[i]));
			}
			clauses.push_back(clause);
		}
		i++;
	}

}
vector<string> split(string line)
{
	std::vector<string> parts;
	istringstream iss(line);
	string s;
	while ( getline( iss, s, ' ' ) ) 
	{
		parts.push_back(s.c_str() );
  	}
	return parts;
}



vector<int> vect_to_int(vector<string> &vect)
{
	vector<int> newvect;
  	newvect.resize(vect.size()); //allocate space
	transform(vect.begin(), vect.end(), newvect.begin(), [](string s) { return stoi(s);});
	return newvect;
}


template<typename K, typename V>
void print_map(map<K,V> const &m)
{
    for (auto const& pair: m) {
        std::cout << "{" << pair.first << ": " << pair.second << "}\n";
    }
}

//			cout << "lines[i] (" << lines[i] << ") is quantified by " << quantifier << endl; 

//			std::cout << "line : " << endl;
//			for(string part : parts)
//			{
//				cout << part << " - ";
//			}
//			cout << endl;	